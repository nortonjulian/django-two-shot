from django.contrib import admin
from .models import ExpenseCategory
from .models import Account
from .models import Receipt

admin.site.register(ExpenseCategory)
admin.site.register(Account)
admin.site.register(Receipt)
